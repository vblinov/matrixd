name := "matrixd"

organization := "org.matrix"

version := "0.1.0-SNAPSHOT"

homepage := Some(url("https://bitbucket.com/vblinov/matrixd"))

startYear := Some(2015)

scmInfo := Some(
  ScmInfo(
    url("https://bitkucket.com/vblinov/matrixd"),
    "scm:git:https://bitbucket.com/vblinov/matrixd.git",
    Some("scm:git:git@bitbucket.com/vblinov/matrixd.git")
  )
)

/* scala versions and options */
scalaVersion := "2.11.7"

crossScalaVersions := Seq(
/*  "2.9.3-RC1",
  "2.9.2",
  "2.9.1", "2.9.1-1",
  "2.9.0", "2.9.0-1",
  "2.8.0", "2.8.1", "2.8.2" */
)

// These options will be used for *all* versions.
scalacOptions ++= Seq(
  "-deprecation"
  ,"-unchecked"
  ,"-encoding", "UTF-8"
  ,"-target:jvm-1.8"
  ,"-Xlint"
  // "-optimise"   // this option will slow your build
)

scalacOptions ++= Seq(
  "-Yclosure-elim"
  , "-Yinline"
  , "-Yinline-warnings"
)

// These language flags will be used only for 2.10.x.
// Uncomment those you need, or if you hate SIP-18, all of them.
scalacOptions <++= scalaVersion map { sv =>
  if (sv startsWith "2.10") List(
    "-Xverify"
    ,"-Ywarn-all"
    ,"-feature"
    ,"-language:postfixOps"
    // "-language:reflectiveCalls",
    // "-language:implicitConversions"
    // "-language:higherKinds",
    // "-language:existentials",
    // "-language:experimental.macros",
    // "-language:experimental.dynamics"
  )
  else Nil
}

javacOptions ++= Seq("-Xlint:unchecked", "-Xlint:deprecation")

val akka = "2.3.12"
val spray = "1.3.3"
val slick = "3.0.1"

/* dependencies */
libraryDependencies ++= Seq (
  "com.github.nscala-time" %% "nscala-time" % "1.0.0"
  // -- testing --
  , "org.scalatest" %% "scalatest" % "2.1.7" % "test"
  //, "org.scalamock" %% "scalamock-scalatest-support" % "3.1.RC1" % "test"
  // -- Logging --
  ,"ch.qos.logback" % "logback-classic" % "1.1.2"
  ,"com.typesafe.scala-logging" %% "scala-logging-slf4j" % "2.1.2"
  // -- Akka --
  ,"com.typesafe.akka" %% "akka-testkit" % akka % "test"
  ,"com.typesafe.akka" %% "akka-actor" % akka
  ,"com.typesafe.akka" %% "akka-slf4j" % akka
  ,"com.typesafe.akka" %% "akka-cluster" % akka
  // -- Spray --
  ,"io.spray" %% "spray-json" % "1.3.2"
  ,"io.spray" %% "spray-routing" % spray
  ,"io.spray" %% "spray-client" % spray
  ,"io.spray" %% "spray-testkit" % spray % "test"
  //-- Slick --
  ,"com.typesafe.slick" %% "slick" % slick
  //-- H2 database (used for initial development & testing) --
  ,"com.h2database" % "h2" % "1.3.170"
  //-- redis --
  , "com.etaty.rediscala" %% "rediscala" % "1.4.0"
  //-- email support --
  ,"org.apache.commons"  % "commons-email"   % "1.2"
  //-- repl --
  ,"com.github.dant3" % "ammonite-sshd" % "09899b1cb9"
)

libraryDependencies += "org.fusesource" % "sigar" % "1.6.4" classifier("native")

/* you may need these repos */
resolvers ++= Seq(
  Resolver.sonatypeRepo("releases"),
  Resolver.typesafeRepo("releases"),
  "spray repo" at "http://nightlies.spray.io",
  "spray" at "http://repo.spray.io/",
  /*rediscala at*/Resolver.bintrayRepo("etaty", "maven"),
  "jitpack" at "https://jitpack.io"
)

seq(Revolver.settings: _*)

testOptions in Test += Tests.Setup(classLoader =>
  classLoader
    .loadClass("org.slf4j.LoggerFactory")
    .getMethod("getLogger", classLoader.loadClass("java.lang.String"))
    .invoke(null, "ROOT")
)

packageArchetype.java_application
