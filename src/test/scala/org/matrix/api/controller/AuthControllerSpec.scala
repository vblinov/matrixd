package org.matrix.api.controller

import akka.actor.ActorDSL._
import akka.actor.ActorRef
import org.matrix.api.login.AuthController
import org.matrix.core.auth.PasswordBasedAuthenticator.Credentials
import org.matrix.core.auth.{AuthEngine, PasswordBasedAuthenticator}
import org.matrix.protocol.base.{ApiError, UserID}
import org.matrix.protocol.login.LoginRequest.AuthData
import org.matrix.protocol.login.{LoginFlows, LoginRequest, LoginResult}
import org.matrix.testkit.{FuturesTestKit, MatrixControllerSpec, TestDatabase}
import org.matrix.util.ReplyingActor
import org.scalatest.Inside
import spray.http.StatusCodes._

import scala.util.Random

class AuthControllerSpec extends MatrixControllerSpec with Inside with FuturesTestKit with TestDatabase {
  val sampleCredentials = PasswordBasedAuthenticator.Credentials("foo", "bar")
  val sampleUserID = UserID("foo", "localhost")

  val controller = new AuthController(new AuthEngine(new PasswordBasedAuthenticator(db) :: Nil), stubSessionManager())


  "The Login Controller" - {
    "get /login" - {
      "returns 200 with LoginFlows" in {
        Get("/login") ~> controller.route ~> check {
          status shouldEqual OK
          val defaultFlow = responseAs[LoginFlows].flows.head
          defaultFlow.`type` shouldEqual PasswordBasedAuthenticator.`type`
        }
      }
    }
    "register using /register, then login using /login" in {
      val authData = AuthData(PasswordBasedAuthenticator.`type`, sampleCredentials.toMap)
      Post("/register", LoginRequest(authData)) ~> controller.route ~> check { loginIsOk() }
      Post("/login", LoginRequest(authData)) ~> controller.route ~> check { loginIsOk() }
    }
    "post /login with invalid creds" - {
      "returns not found error" in {
        Post("/login", LoginRequest(AuthData(PasswordBasedAuthenticator.`type`, Credentials("invalid", "login").toMap))) ~>
            controller.route ~> check {
          status shouldEqual OK
          responseAs[ApiError] shouldEqual ApiError.notFound
        }
      }
    }
  }


  def loginIsOk() = {
    status shouldEqual OK
    inside(responseAs[LoginResult]) { case LoginResult(Left(LoginResult.Success(token, userID))) ⇒
      userID shouldEqual sampleUserID
    }
  }


  private def stubSessionManager():ActorRef = actor("test-auth-token-manager")(new ReplyingActor {
    import org.matrix.core.auth.AuthTokenManagerActor._
    private val stubToken = Random.nextString(16)

    override def respond = {
      case AcquireToken(userID:UserID) if userID == sampleUserID ⇒ stubToken
      case IdentifyToken(token) if token == stubToken ⇒ Some(sampleUserID)
      case any ⇒ ???
    }
  })
}

