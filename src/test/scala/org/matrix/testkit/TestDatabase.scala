package org.matrix.testkit

import org.matrix.core.db.{TempH2Connection, Database}

trait TestDatabase {
  lazy val db = new Database with TempH2Connection {
    runBlocking(createSchema)
  }
}
