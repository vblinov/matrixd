package org.matrix.testkit

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.language.postfixOps

trait FuturesTestKit {
  implicit val defaultTimeout = 15 seconds

  def resultOf[T](future:Future[T])(implicit timeout:FiniteDuration) = Await.result(future, timeout)
}
