package org.matrix.testkit

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}

class ActorsTestKit extends TestKit(ActorSystem("matrix-actor-spec")) with ImplicitSender with FuturesTestKit {
  implicit val executionContext = system.dispatcher
}
