package org.matrix.testkit

import org.matrix.protocol.MatrixJsonProtocol
import org.scalatest.{FreeSpec, Matchers}
import spray.httpx.SprayJsonSupport
import spray.testkit.ScalatestRouteTest

trait MatrixControllerSpec extends FreeSpec with ScalatestRouteTest with Matchers with SprayJsonSupport with MatrixJsonProtocol
