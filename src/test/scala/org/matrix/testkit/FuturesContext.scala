package org.matrix.testkit

trait FuturesContext {
  implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global
}
