package org.matrix.testkit

import org.scalatest.{BeforeAndAfterEach, Suite}
import redis.RedisClient

import scala.concurrent.{Future, Await}
import scala.concurrent.duration._
import scala.language.postfixOps

trait RedisTestkit extends Suite with BeforeAndAfterEach { self:ActorsTestKit ⇒
  val redisKeyPrefix = "org.matrix.testkit"
  val redis = new RedisClient()

  def redisKeyPrefix(name:String):String = s"$redisKeyPrefix:$name"

  override def beforeEach() {
    Await.result(removeTestPrefixKeys(), 30 seconds)
  }

  private def removeTestPrefixKeys():Future[Long] = {
    redis.keys(s"$redisKeyPrefix:*").flatMap { keys ⇒
      if (keys.isEmpty) Future.successful(0L) else redis.del(keys:_*)
    }
  }
}
