package org.matrix.core.auth

import akka.actor.Props
import org.matrix.core.auth.AuthTokenManagerActor.{AcquireToken, IdentifyToken}
import org.matrix.protocol.base.UserID
import org.matrix.testkit.{ActorsTestKit, RedisTestkit}
import org.scalatest.{Matchers, WordSpecLike}

import scala.language.postfixOps
import scala.util.Random

class AuthTokenManagerActorSpec extends ActorsTestKit with WordSpecLike with Matchers with RedisTestkit {
  val keyPrefix = redisKeyPrefix("session_manager")
  val actor = system.actorOf(Props(new AuthTokenManagerActor(redis)(keyPrefix)))
  val randomUser = UserID("test", "localhost")

  "Auth Token Manager Actor" should {
    "create new auth tokens" in {
      actor ! AcquireToken(randomUser)
      val authToken = expectMsgType[String]

      resultOf(readUserAuth(keyPrefix, authToken)) shouldEqual randomUser
    }
    "read existing session with updating their ttl" in {
      val authToken = Random.alphanumeric.take(10).mkString
      val authTokenTTL = 5L
      val tokenRedisKey = s"$keyPrefix:$authToken"
      redis.setex(tokenRedisKey, authTokenTTL, randomUser.toString)
      actor ! IdentifyToken(authToken)
      expectMsgType[Option[UserID]] shouldBe Some(randomUser)
      resultOf(redis.ttl(tokenRedisKey)) should be > authTokenTTL
    }
  }

  private def readUserAuth(prefix:String, sessionID:String) = {
    redis.get[String](s"$prefix:$sessionID").map(_.get).map(UserID(_))
  }
}
