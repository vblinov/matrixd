package org.matrix.protocol

import spray.json.DeserializationException

class JsonParsingException extends DeserializationException("Got illegal json")