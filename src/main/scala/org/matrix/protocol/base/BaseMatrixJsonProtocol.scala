package org.matrix.protocol.base

import org.matrix.protocol.JsonConversions
import spray.json.{JsonFormat, DefaultJsonProtocol}

trait BaseMatrixJsonProtocol extends DefaultJsonProtocol with JsonConversions
{
  implicit val throwableFormat = new ThrowableWriter
  implicit val errorFormat = new ApiError.Format
  implicit val userIDFormat = UserID.format
  implicit val roomIDFormat = RoomID.format
  implicit def paginationFormat[Token : JsonFormat, Item : JsonFormat] = PaginationChunkResponse.format[Token, Item]
}
