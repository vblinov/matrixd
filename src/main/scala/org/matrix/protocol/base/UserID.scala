package org.matrix.protocol.base

import org.matrix.protocol.JsonParsingException
import org.matrix.util.RegexMatching
import spray.json.{JsString, JsValue, JsonFormat}

final case class UserID(localpart:String, domain:String) {
  override def hashCode = localpart.hashCode + 31 * domain.hashCode

  override def equals(other:Any):Boolean = other match {
    case otherID: UserID ⇒
      domain.equalsIgnoreCase(otherID.domain) && localpart.equalsIgnoreCase(otherID.localpart)
    case otherObject ⇒ super.equals(otherObject)
  }

  override def toString = s"@$localpart:$domain"
}

object UserID extends RegexMatching {
  def apply(string:String):UserID = string match {
    case r"@$localpart:$domain" ⇒ UserID(localpart, domain)
  }

  val format = new JsonFormat[UserID] {
    override def read(json: JsValue): UserID = json match {
      case JsString(value) ⇒ UserID(value)
      case _ ⇒ throw new JsonParsingException
    }
    override def write(obj: UserID): JsValue = JsString(obj.toString)
  }
}