package org.matrix.protocol.base

import org.matrix.protocol.JsonParsingException
import org.matrix.util.RegexMatching
import spray.json.{JsString, JsValue, JsonFormat}

final case class RoomAlias(localpart:String, domain:String) {
  override def hashCode = localpart.hashCode + 31 * domain.hashCode

  override def equals(other:Any):Boolean = other match {
    case otherID: RoomAlias ⇒
      domain.equalsIgnoreCase(otherID.domain) && localpart.equalsIgnoreCase(otherID.localpart)
    case otherObject ⇒ super.equals(otherObject)
  }

  override def toString = s"#$localpart:$domain"
}

object RoomAlias extends RegexMatching {
  def apply(string:String):RoomAlias = string match {
    case r"#$localpart:$domain" ⇒ RoomAlias(localpart, domain)
  }

  val format = new JsonFormat[RoomAlias] {
    override def read(json: JsValue): RoomAlias = json match {
      case JsString(value) ⇒ RoomAlias(value)
      case _ ⇒ throw new JsonParsingException
    }
    override def write(obj: RoomAlias): JsValue = JsString(obj.toString)
  }
}