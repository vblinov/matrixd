package org.matrix.protocol.base

import spray.json._

class ThrowableWriter extends JsonWriter[Throwable] {
  override def write(error: Throwable): JsValue = JsObject(Map(
    "exception"  -> JsString(error.getClass.getCanonicalName),
    "stacktrace" -> writeStacktrace(error.getStackTrace)
  ) ++ Option(error.getMessage).map("message" → JsString(_)) ++ Option(error.getCause).map("cause" → write(_)))

  private def writeStacktrace(stacktrace: Array[StackTraceElement]) =
    JsArray(stacktrace.map { stElement => JsString(stElement.toString) }.toVector)
}
