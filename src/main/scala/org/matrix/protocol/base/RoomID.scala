package org.matrix.protocol.base

import org.matrix.protocol.JsonParsingException
import org.matrix.util.RegexMatching
import spray.json.{JsString, JsValue, JsonFormat}

final case class RoomID(localpart:String, domain:String) {
  override def hashCode = localpart.hashCode + 31 * domain.hashCode

  override def equals(other:Any):Boolean = other match {
    case otherID: RoomID ⇒
      domain.equalsIgnoreCase(otherID.domain) && localpart.equalsIgnoreCase(otherID.localpart)
    case otherObject ⇒ super.equals(otherObject)
  }

  override def toString = s"!$localpart:$domain"
}

object RoomID extends RegexMatching {
  def apply(string:String):RoomID = string match {
    case r"!$localpart:$domain" ⇒ RoomID(localpart, domain)
  }

  val format = new JsonFormat[RoomID] {
    override def read(json: JsValue): RoomID = json match {
      case JsString(value) ⇒ RoomID(value)
      case _ ⇒ throw new JsonParsingException
    }
    override def write(obj: RoomID): JsValue = JsString(obj.toString)
  }
}