package org.matrix.protocol.base

import org.matrix.protocol.JsonParsingException
import spray.json._

case class ApiError(code:String, message:String, cause:Option[Throwable] = None) {
  def withCause(cause:Throwable) = ApiError(code, message, Some(cause))
}

object ApiError {
  def forbidden = ApiError("M_FORBIDDEN", "Forbidden access")
  def unknownToken = ApiError("M_UNKNOWN_TOKEN", "The access token specified was not recognised.")
  def badJson = ApiError("M_BAD_JSON", "Request contained valid JSON, but it was malformed")
  def notJson = ApiError("M_NOT_JSON", "Request did not contain valid JSON")
  def notFound = ApiError("M_NOT_FOUND", "No resource was found for this request")
  def limitExceeded = ApiError("M_LIMIT_EXCEEDED", "Too many requests have been sent in a short period of time. Wait a while then try again.")
  def userExists = ApiError("M_USER_IN_USE", "Trying to register a user ID which has been taken.")
  def badPagination = ApiError("M_BAD_PAGINATION", "Bad pagination query parameters")
  def internalError(error:Throwable) = ApiError("M_INTERNAL_ERROR", "Internal server error", Some(error))

  class Format(implicit throwableWriter: JsonWriter[Throwable], stringReader:JsonReader[String]) extends RootJsonFormat[ApiError] {
    override def read(json: JsValue): ApiError = json match {
      case obj:JsObject ⇒
        val Seq(JsString(code), JsString(message)) = obj.getFields("code", "message")
        ApiError(code, message)
      case _ ⇒ throw new JsonParsingException
    }

    override def write(error: ApiError): JsValue = JsObject(Map(
      "code" → JsString(error.code),
      "message" → JsString(error.message)
    ) ++ error.cause.map("cause" → throwableWriter.write(_)) )
  }
}

// Some requests have unique error codes:
//  M_USER_IN_USE:  Encountered when trying to register a user ID which has been taken.
//  M_ROOM_IN_USE:  Encountered when trying to create a room which has been taken.
//  M_BAD_PAGINATION:
//  Encountered when specifying bad pagination query parameters.
//      M_LOGIN_EMAIL_URL_NOT_YET:
//      Encountered when polling for an email link which has not been clicked yet.