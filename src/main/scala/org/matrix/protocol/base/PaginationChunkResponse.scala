package org.matrix.protocol.base

import spray.json.{JsonFormat, DefaultJsonProtocol}

case class PaginationChunkResponse[TokenType, ItemType](chunk:Seq[ItemType], start:TokenType, end:TokenType)

object PaginationChunkResponse {
  import DefaultJsonProtocol._

  def format[Token : JsonFormat, Item : JsonFormat] = jsonFormat3(PaginationChunkResponse.apply[Token, Item])
}