package org.matrix.protocol

import spray.json

trait JsonConversions {
  implicit def pimpAny[T](any: T) = json.pimpAny[T](any)
  implicit def pimpString(string: String) = json.pimpString(string)
}
