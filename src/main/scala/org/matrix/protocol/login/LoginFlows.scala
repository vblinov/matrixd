package org.matrix.protocol.login

case class LoginFlows(flows:Seq[LoginInfo], session:Option[String] = None) {
  def withSession(session:String) = LoginFlows(flows, Some(session))
}
