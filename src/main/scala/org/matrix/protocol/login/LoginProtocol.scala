package org.matrix.protocol.login

import org.matrix.protocol.base.BaseMatrixJsonProtocol
import spray.json.RootJsonFormat

trait LoginProtocol extends BaseMatrixJsonProtocol {
  implicit val authDataFormat:RootJsonFormat[LoginRequest.AuthData] = LoginRequest.authDataFormat
  implicit val loginInfoFormat:RootJsonFormat[LoginInfo] = jsonFormat2(LoginInfo.apply)
  implicit val loginFlowsFormat:RootJsonFormat[LoginFlows] = jsonFormat2(LoginFlows.apply)
  implicit val loginRequestFormat:RootJsonFormat[LoginRequest] = jsonFormat1(LoginRequest.apply)
  implicit val loginResultFormat:RootJsonFormat[LoginResult] = LoginResult.format
}
