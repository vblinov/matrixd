package org.matrix.protocol.login

import org.matrix.protocol.login.LoginRequest.AuthData
import spray.json._

case class LoginRequest(auth:AuthData)

object LoginRequest {
  case class AuthData(`type`:String, credentials:Map[String, String] = Map.empty, session:Option[String] = None)

  val authDataFormat = new RootJsonFormat[AuthData] {
    override def read(json: JsValue): AuthData = {
      val fields = json.asJsObject.fields
      val session = fields.get("session").map(jsToString)
      val credentials = fields - "type" - "session"
      fields.get("type") match {
        case Some(JsString(authType)) ⇒ AuthData(authType, credentials.mapValues(jsToString), session)
        case _ ⇒ throw new DeserializationException("""expected "type" field with string value""")
      }
    }
    override def write(obj: AuthData): JsObject = JsObject(
      obj.credentials.mapValues(JsString(_)) + ("type" → JsString(obj.`type`)) ++ obj.session.map("session" → JsString(_))
    )

    private val jsToString:JsValue ⇒ String = {
      case JsString(value) ⇒ value
      case _ ⇒ throw new DeserializationException("expected string json value")
    }
  }
}
