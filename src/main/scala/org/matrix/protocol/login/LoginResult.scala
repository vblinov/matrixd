package org.matrix.protocol.login

import org.matrix.protocol.{JsonParsingException, JsonConversions}
import org.matrix.protocol.base.UserID
import org.matrix.protocol.login.LoginResult._
import spray.json._

case class LoginResult(value:Either[Success, NextStage])

object LoginResult {
  case class Success(token:String, userID:UserID)
  case class NextStage(stage:String, session:String)

  def successResult(token:String, userID:UserID) = Left(Success(token, userID))
  def nextStageResult(stage:String, session:String) = Right(NextStage(stage, session))
  def success(token:String, userID:UserID):LoginResult = LoginResult(successResult(token, userID))
  def proceedToNextStage(stage:String, session:String) = LoginResult(nextStageResult(stage, session))

  def format(implicit fmt:JsonFormat[UserID]) = new RootJsonFormat[LoginResult] with JsonConversions {
    override def write(result: LoginResult): JsValue = JsObject(result.value match {
      case Left(Success(token, userID)) ⇒ Map("access_token" → JsString(token), "user_id" → userID.toJson)
      case Right(NextStage(stage, session)) ⇒ Map("next" → JsString(stage), "session" → JsString(session))
    })
    override def read(value: JsValue): LoginResult = {
      val fields = value.asJsObject.fields
      (successFields(fields), nextStageFields(fields)) match {
        case (Some((JsString(token), userID)), _) ⇒ success(token, userID.convertTo[UserID])
        case (None, Some((JsString(next), JsString(session)))) ⇒ proceedToNextStage(next, session)
        case _ ⇒ throw new JsonParsingException
      }
    }

    def successFields(fields:Map[String, JsValue]) = zip(fields.get("access_token"), fields.get("user_id"))
    def nextStageFields(fields:Map[String, JsValue]) = zip(fields.get("access_next"), fields.get("session"))

    private def zip[T,U](opt1:Option[T], opt2:Option[U]):Option[(T,U)] = for (val1 ← opt1; val2 ← opt2) yield (val1, val2)
  }
}
