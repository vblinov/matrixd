package org.matrix.protocol

import org.matrix.protocol.base.BaseMatrixJsonProtocol
import org.matrix.protocol.login.LoginProtocol

import scala.language.implicitConversions

trait MatrixJsonProtocol extends JsonConversions
  with BaseMatrixJsonProtocol
  with LoginProtocol