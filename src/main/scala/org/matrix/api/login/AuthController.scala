package org.matrix.api.login

import akka.actor.ActorRef
import akka.pattern.ask
import org.matrix.api.Controller
import org.matrix.core.auth.{AuthTokenManagerActor, AuthEngine}
import org.matrix.protocol.MatrixJsonProtocol
import org.matrix.protocol.base.{UserID, ApiError}
import org.matrix.protocol.login.{LoginRequest, LoginResult}
import spray.routing.Route

import scala.concurrent.{Future, ExecutionContext}

class AuthController(authEngine:AuthEngine, authTokenManager: ActorRef)(implicit ctx:ExecutionContext)
      extends Controller with MatrixJsonProtocol {
  override def route = path("login") {
    get { completeWithJson(authEngine.description) } ~ jsonPost(handleLoginRequest)
  } ~ path("register") {
    get { completeWithJson(authEngine.description) } ~ jsonPost(handleRegisterRequest)
  }


  def handleRegisterRequest(registerRequest:LoginRequest):Route = {
    onSuccess(authEngine.register(registerRequest.auth)) {
      case Some(userID) ⇒
        onSuccess(acquireAuthToken(userID)) { authToken ⇒
          completeWithJson(LoginResult.success(authToken, userID))
        }
      case None ⇒
        completeWithJson(ApiError.userExists)
    }
  }

  def handleLoginRequest(loginRequest:LoginRequest):Route = {
    onSuccess(authEngine.challenge(loginRequest.auth)) {
      case Some(userID) ⇒
        onSuccess(acquireAuthToken(userID)) { authToken ⇒
          completeWithJson(LoginResult.success(authToken, userID))
        }
      case None ⇒
        completeWithJson(ApiError.notFound)
    }
  }

  private def acquireAuthToken(user:UserID):Future[String] = {
    authTokenManager.ask(AuthTokenManagerActor.AcquireToken(user)).mapTo[String]
  }
}
