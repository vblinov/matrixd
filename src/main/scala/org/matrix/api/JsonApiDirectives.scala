package org.matrix.api

import spray.http.MediaTypes._
import spray.httpx.SprayJsonSupport
import spray.json.{JsonWriter, _}
import spray.routing.{Directives, Route}

trait JsonApiDirectives extends Directives with SprayJsonSupport {
  final def completeWithJson[T : JsonWriter](any: T) = respondWithMediaType(`application/json`) {
    complete(any.toJson.prettyPrint)
  }

  def jsonPost[T : RootJsonReader](handler:T ⇒ Route) = post { entity(as[T])(handler) }
}
