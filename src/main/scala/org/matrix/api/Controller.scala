package org.matrix.api

import akka.util.Timeout
import spray.routing.Route
import scala.concurrent.duration._
import scala.language.postfixOps

trait Controller extends JsonApiDirectives {
  implicit val responseTimeout = 60 seconds
  implicit final def responseTimeoutForActors = Timeout(responseTimeout)
  def route:Route
}
