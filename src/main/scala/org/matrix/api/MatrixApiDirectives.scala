package org.matrix.api

import akka.util.Timeout
import org.matrix.core.Core
import org.matrix.core.auth.AuthTokenManagerActor.IdentifyToken
import org.matrix.protocol.base.{ApiError, UserID}
import spray.routing.Route
import spray.routing.directives.ParameterDirectives
import akka.pattern.ask

trait MatrixApiDirectives extends ParameterDirectives with JsonApiDirectives {
  def core:Core
  implicit def executionContext = core.actorSystem.dispatcher
  implicit def timeout:Timeout = ???

  def paginatedDataRequest(handler:(String, String, Int) ⇒ Route) = parameters('from, 'to, 'limit.as[Int])(handler)

  def authenticatedRequest(handler:UserID ⇒ Route) = parameters('access_token) { accessToken:String ⇒
    onSuccess(core.authTokenManager ? IdentifyToken(accessToken)) {
      case Some(user:UserID) ⇒ handler(user)
      case _ ⇒ completeWithJson(ApiError.forbidden)
    }
  }
}
