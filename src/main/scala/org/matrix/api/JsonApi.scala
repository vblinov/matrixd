package org.matrix.api

import org.matrix.api.login.AuthController
import org.matrix.core.Core
import org.matrix.protocol.base.ApiError
import spray.http.StatusCodes
import spray.json.DeserializationException
import spray.routing.{ExceptionHandler, HttpService}


trait JsonApi extends HttpService with JsonApiDirectives {
  def core:Core
  implicit private def executionContext = actorRefFactory.dispatcher

  private lazy val controllers:List[Controller] = new AuthController(core.authEngine, core.authTokenManager) :: Nil
  private def routes = controllers.view.map(_.route)

  lazy val serveApi = handleExceptions(exceptionsHandler) {
    routes.foldRight(notFoundError)(_ ~ _)
  }

  private val exceptionsHandler = ExceptionHandler {
    case parsingError:DeserializationException ⇒ completeWithJson(ApiError.badJson.withCause(parsingError))
  }

  private def notFoundError = respondWithStatus(StatusCodes.NotFound) { getFromResource("404.html") }
}
