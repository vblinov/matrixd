package org.matrix.core

import org.matrix.core.db.Database

trait DatabaseAccess {
  val db:Database
}
