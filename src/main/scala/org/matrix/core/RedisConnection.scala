package org.matrix.core

import redis.RedisClient

trait RedisConnection { self:Core ⇒
  lazy val redis = new RedisClient()
}
