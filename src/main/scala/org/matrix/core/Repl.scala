package org.matrix.core

import ammonite.sshd.{SshServerConfig, SshdRepl}
import org.matrix.ServerConfig
import org.matrix.util.Server


trait Repl extends Server {
  private lazy val sshdConfig = SshServerConfig(port = ServerConfig.Repl.port, ServerConfig.Repl.credentials :: Nil)
  lazy val sshd = new SshdRepl(sshdConfig)

  abstract override def start() = {
    sshd.start()
    println(s"sshd repl started on port ${sshdConfig.port}, creds = ${sshdConfig.users}")
    super.start()
  }

  abstract override def shutdown() = {
    sshd.stop()
    super.shutdown()
  }
}
