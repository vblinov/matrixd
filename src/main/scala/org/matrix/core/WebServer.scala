package org.matrix.core

import akka.actor.ActorDSL._
import akka.io.IO
import org.matrix.core.http.{MatrixJsonApiService, HttpLogger}
import org.matrix.ServerConfig
import org.matrix.util.Server
import spray.can.Http

trait WebServer extends Server { self:Core ⇒
  lazy val httpLoggerService = actor(new HttpLogger)
  lazy val jsonApiService = actor("matrix-api")(new MatrixJsonApiService(this))
  
  private def httpServer = IO(Http)

  abstract override def start() = {
    implicit val listener = httpLoggerService
    httpServer ! Http.Bind(jsonApiService, interface = "localhost", port = ServerConfig.Http.port)
    println("Http server started")
    super.start()
  }  
  
  abstract override def shutdown() = {
    implicit val listener = httpLoggerService
    httpServer ! Http.Unbind
    super.shutdown()
  }
}
