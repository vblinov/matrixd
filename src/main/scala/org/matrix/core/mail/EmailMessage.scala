package org.matrix.core.mail

import scala.concurrent.duration.FiniteDuration

case class EmailMessage(subject: String,
                        recipient: String,
                        from: String,
                        text: Option[String] = None,
                        html: Option[String] = None,
                        smtpConfig: SmtpConfig,
                        retryOn: FiniteDuration,
                        var deliveryAttempts: Int = 0)
