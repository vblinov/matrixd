package org.matrix.core.mail

case class SmtpConfig(tls: Boolean = false,
                      ssl: Boolean = false,
                      port: Int = 2525,
                      host: String,
                      user: String,
                      password: String)