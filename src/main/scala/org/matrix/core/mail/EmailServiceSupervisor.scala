package org.matrix.core.mail

import akka.actor.SupervisorStrategy.{Stop, Restart}
import akka.actor.{Props, OneForOneStrategy, ActorLogging, Actor}
import org.apache.commons.mail.EmailException

class EmailServiceSupervisor extends Actor with ActorLogging {

  /**
   * The actor supervisor strategy attempts to send email up to 10 times if there is a EmailException
   */
  override val supervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 10) {
      case emailException: EmailException =>
        log.debug("Restarting after receiving EmailException : {}", emailException.getMessage)
        Restart
      case unknownException: Exception =>
        log.debug("Giving up. Can you recover from this? : {}", unknownException)
        Stop
      case unknownCase: Any =>
        log.debug("Giving up on unexpected case : {}", unknownCase)
        Stop
    }

  /**
   * Forwards messages to child workers
   */
  def receive = {
    case message: Any => context.actorOf(Props[EmailServiceWorker]) ! message
  }
}
