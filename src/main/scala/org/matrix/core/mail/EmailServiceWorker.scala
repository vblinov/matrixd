package org.matrix.core.mail

import akka.actor.{ActorLogging, Actor}
import org.apache.commons.mail.{DefaultAuthenticator, HtmlEmail}

/**
 * Email worker that delivers the message
 */
class EmailServiceWorker extends Actor with ActorLogging {

  /**
   * The email message in scope
   */
  private var emailMessage: Option[EmailMessage] = None

  /**
   * Delivers a message
   */
  def receive = {
    case email: EmailMessage =>
      emailMessage = Option(email)
      email.deliveryAttempts = email.deliveryAttempts + 1
      log.info("Attempting to deliver message to {}:{}", email.smtpConfig.host, email.smtpConfig.port )
      sendEmailSync(email)
      log.info("Message delivered")
      emailMessage = None
    case unexpectedMessage: Any =>
      log.error("Received unexpected message : {}", unexpectedMessage)
      throw new Exception("can't handle %s".format(unexpectedMessage))
  }

  /**
   * If this child has been restarted due to an exception attempt redelivery
   * based on the message configured delay
   */
  override def preRestart(reason: Throwable, message: Option[Any]) {
    if (emailMessage.isDefined) {
      log.debug("Scheduling email message to be sent after attempts: {}", emailMessage.get)
      import context.dispatcher
      // Use this Actors' Dispatcher as ExecutionContext

      context.system.scheduler.scheduleOnce(emailMessage.get.retryOn, self, emailMessage.get)
    }
  }

  override def postStop() {
    if (emailMessage.isDefined) {
      log.debug("Stopped child email worker after attempts {}, {}", emailMessage.get.deliveryAttempts, self)
    }
  }

  /**
   * Private helper invoked by the actors that sends the email
   * @param emailMessage the email message
   */
  private def sendEmailSync(emailMessage: EmailMessage) {
    // Create the email message
    val email = new HtmlEmail()
    email.setTLS(emailMessage.smtpConfig.tls)
    email.setSSL(emailMessage.smtpConfig.ssl)
    email.setSmtpPort(emailMessage.smtpConfig.port)
    email.setHostName(emailMessage.smtpConfig.host)
    email.setAuthenticator(new DefaultAuthenticator(
      emailMessage.smtpConfig.user,
      emailMessage.smtpConfig.password
    ))

    emailMessage.text match {
      case Some(text) => email.setTextMsg(text)
      case None =>
    }

    emailMessage.html match {
      case Some(html) => email.setHtmlMsg(html)
      case None =>
    }

    email.addTo(emailMessage.recipient)
        .setFrom(emailMessage.from)
        .setSubject(emailMessage.subject)
        .send()
  }
}
