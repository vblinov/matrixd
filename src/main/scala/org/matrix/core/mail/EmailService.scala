package org.matrix.core.mail

import akka.actor._
import akka.routing.SmallestMailboxPool

/**
 * Email service facade
 */
object EmailService {
 /**
   * Uses the smallest inbox strategy to keep 20 instances alive ready to send out email
   * @see SmallestMailboxRouter
   */
  val actor = akka.actor.ActorSystem("system").actorOf(
    Props[EmailServiceSupervisor].withRouter(
      SmallestMailboxPool(nrOfInstances = 50)
    ), name = "emailService"
  )


  /**
   * public interface to send out emails that dispatch the message to the listening actors
   * @param emailMessage the email message
   */
  def send(emailMessage: EmailMessage) {
    actor ! emailMessage
  }
}