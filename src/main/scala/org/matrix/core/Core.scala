package org.matrix.core

import akka.actor.{ActorSystem, Props}
import org.matrix.core.auth.{AuthTokenManagerActor, AuthEngine, PasswordBasedAuthenticator}
import org.matrix.util.{Server, UserCli}

trait Core extends Server with UserCli { self:DatabaseAccess with RedisConnection ⇒
  implicit def actorSystem: ActorSystem
  implicit def executionContext = actorSystem.dispatcher

  lazy val authTokenManager = actorSystem.actorOf(Props(new AuthTokenManagerActor(redis)("auth_token_v1")))
  lazy val authEngine = new AuthEngine(new PasswordBasedAuthenticator(db) :: Nil)

  override def start() = {
    println("Server started")
    awaitForShutdownCommand(this)
  }

  override def shutdown() = {
    actorSystem.shutdown()
    println("Server stopped")
  }
}