package org.matrix.core.db

import slick.driver.H2Driver
import slick.jdbc.JdbcBackend

trait TempH2Connection extends ConnectionDefinition { self:Database ⇒
  override protected lazy val driver = H2Driver
  override protected lazy val databaseConnection = {
    org.h2.Driver.load()
    JdbcBackend.Database.forURL(
      s"jdbc:h2:mem:test1;${h2options(keepData = true)}",
      driver = driverName[org.h2.Driver]
    )
  }

  private def h2options(keepData:Boolean) = if (keepData) "DB_CLOSE_DELAY=-1" else ""
}
