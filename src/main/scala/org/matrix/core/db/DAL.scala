package org.matrix.core.db

import org.matrix.core.db.table.UsersTable

trait DAL extends UsersTable { this: DriverDefinition ⇒
  import driver.api._
  
  private def schema = users.schema

  def createSchema = schema.create
  def dropSchema = schema.drop
}
