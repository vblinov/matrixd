package org.matrix.core.db.table

import java.sql.Timestamp

import org.joda.time.DateTime
import org.matrix.core.db.DriverDefinition
import org.matrix.protocol.base.UserID

trait ColumnMappers { this:DriverDefinition ⇒
  import driver.api._

  implicit def _mapDateTime = MappedColumnType.base[DateTime, Timestamp](
    dt => new Timestamp(dt.getMillis),
    ts => new DateTime(ts.getTime)
  )

  implicit def _mapUserID = MappedColumnType.base[UserID, String](
    _.toString,
    UserID.apply
  )
}
