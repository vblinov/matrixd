package org.matrix.core.db.table

import org.joda.time.DateTime
import org.matrix.core.db.DriverDefinition
import org.matrix.core.db.data.User
import org.matrix.protocol.base.UserID

trait UsersTable extends ColumnMappers { this:DriverDefinition ⇒
  import driver.api._

  class Users(tag:Tag) extends Table[User](tag, "users") {
    def id = column[UserID]("id", O.PrimaryKey)
    def username = column[String]("username")
    def password = column[String]("password")
    def registrationDate = column[DateTime]("registrationDate")
    def * = (id, username, password, registrationDate) <> (User.tupled, User.unapply)
    def usernameIndex =  index("idx_username", username, unique = true)
  }

  val users = TableQuery[Users]

  def findUserByUsername(username:String) = users.filter(_.username === username).take(1).result.headOption
}