package org.matrix.core.db.data

import org.joda.time.DateTime
import org.matrix.protocol.base.UserID

case class User(
  id:UserID,
  username:String,
  password:String,
  registrationDate: DateTime
)