package org.matrix.core.db

import slick.jdbc.JdbcBackend

trait ConnectionDefinition extends DriverDefinition {
  protected def databaseConnection: JdbcBackend.Database
}
