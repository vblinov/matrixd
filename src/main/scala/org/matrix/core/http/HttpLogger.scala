package org.matrix.core.http

import akka.actor.ActorDSL._
import akka.actor.ActorLogging
import akka.io.Tcp.{Bound, CommandFailed}

class HttpLogger extends Act with ActorLogging {
  become {
    case bound @ Bound(connection) => log.info(bound.toString)
    case commandFailed @ CommandFailed(command) => log.error(commandFailed.toString)
  }
}
