package org.matrix.core.http

import akka.pattern.ask
import spray.can.Http
import spray.can.server.Stats
import spray.http.ContentTypes
import spray.httpx.marshalling.Marshaller
import spray.routing.HttpService
import spray.util._

import scala.concurrent.duration._
import scala.language.{implicitConversions, postfixOps}

trait SprayStats extends HttpService {
  implicit private val ctx = actorSystem.dispatcher
  implicit private val statsMarshaller: Marshaller[Stats] =
    Marshaller.delegate[Stats, String](ContentTypes.`text/plain`)(printStats _)

  def statsRoute = get { path("stats") { complete(getStats) } }

  private def getStats = actorRefFactory.actorSelection("/user/IO-HTTP/listener-0")
        .ask(Http.GetStats)(2 seconds).mapTo[Stats]

  private def printStats(stats:Stats):String =
    s"""Uptime                : ${stats.uptime.formatHMS}
       |Total requests        : ${stats.totalRequests}
       |Open requests         : ${stats.openRequests}
       |Max open requests     : ${stats.maxOpenRequests}
       |Total connections     : ${stats.totalConnections}
       |Open connections      : ${stats.openConnections}
       |Max open connections  : ${stats.maxOpenConnections}
       |Requests timed out    : ${stats.requestTimeouts}
       |""".stripMargin
}
