package org.matrix.core.http

import org.matrix.api.JsonApi
import org.matrix.core.Core
import spray.routing.HttpServiceActor

class MatrixJsonApiService(appCore:Core) extends HttpServiceActor with JsonApi with SprayStats {
  override def receive = runRoute(statsRoute ~ serveApi)
  override def core = appCore
}
