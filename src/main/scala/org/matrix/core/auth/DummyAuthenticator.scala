package org.matrix.core.auth

import java.util.UUID

import org.matrix.protocol.base.UserID

import scala.concurrent.Future

class DummyAuthenticator extends Authenticator {
  override val `type` = "m.login.dummy"
  private def randomString = UUID.randomUUID().toString

  override def challenge(credentials: Map[String, String]): Future[Option[UserID]] = Future.successful(None)
  override def register(credentials: Map[String, String]): Future[Option[UserID]] = Future.failed(new NotImplementedError())
}
