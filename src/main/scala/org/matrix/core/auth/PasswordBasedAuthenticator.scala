
package org.matrix.core.auth

import org.joda.time.DateTime
import org.matrix.ServerConfig
import org.matrix.core.db.Database
import org.matrix.core.db.data.User
import org.matrix.protocol.base.UserID
import spray.json.DeserializationException

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

class PasswordBasedAuthenticator(db:Database)(implicit exec:ExecutionContext) extends Authenticator {
  import PasswordBasedAuthenticator._

  override def `type`: String = PasswordBasedAuthenticator.`type`

  override def challenge(credentials: Map[String, String]):Future[Option[UserID]] = {
    readCredentials(credentials) match {
      case Some(creds) ⇒
        db.run(db.findUserByUsername(creds.login)).map(_.filter(_.password == creds.password).map(_.id))
      case None ⇒ Future.failed(new DeserializationException("Invalid credentials"))
    }
  }

  override def register(credentials: Map[String, String]): Future[Option[UserID]] = {
    readCredentials(credentials) match {
      case Some(creds) ⇒
        import db.api._
        val userID = UserID(creds.login, ServerConfig.domain)
        val insertionResult = db.run(db.users += User(userID, creds.login, creds.password, DateTime.now()))
        insertionResult.map(_ ⇒ Some(userID)).recover { case _ ⇒ None }
      case None ⇒
        Future.failed(new DeserializationException("Invalid credentials"))
    }
  }
}

object PasswordBasedAuthenticator {
  val `type` = "m.login.password"

  case class Credentials(login:String, password:String) {
    def toMap = Map("user" → login, "password" → password)
  }

  def readCredentials(credentials: Map[String, String]):Option[Credentials] = for {
    login ← credentials.get("user")
    password ← credentials.get("password")
  } yield Credentials(login, password)

  private def toTry[T](opt:Option[T], failure: ⇒ Throwable):Try[T] = opt match {
    case Some(data) ⇒ Success(data)
    case None       ⇒ Failure(failure)
  }
}
