package org.matrix.core.auth

import java.util.UUID

import akka.actor.{Actor, ActorLogging}
import akka.pattern._
import org.matrix.core.auth.AuthTokenManagerActor.{IdentifyToken, AcquireToken}
import org.matrix.protocol.base.UserID
import redis.RedisCommands
import redis.commands.Transactions

import scala.concurrent.Future


object AuthTokenManagerActor {
  case class AcquireToken(user:UserID)
  case class IdentifyToken(sessionID:String)
}

class AuthTokenManagerActor(redis:RedisCommands with Transactions)(prefix:String) extends Actor with ActorLogging {
  implicit val executionContext = context.dispatcher
  val sessionTimeout = 60 * 15

  override def receive = {
    case AcquireToken(userID) ⇒
      createSession(userID) pipeTo sender()
    case IdentifyToken(sessionID) ⇒
      findUserSession(sessionID) pipeTo sender()
  }

  private def createSession(userID: UserID):Future[String] = {
    val newSession = generateSessionID(userID)
    for {
      keyUpdated ← redis.setex(redisKey(newSession), sessionTimeout, userID.toString)
      if keyUpdated
    } yield newSession
  }

  private def findUserSession(sessionID:String) = {
    val sessionKey = redisKey(sessionID)
    for {
      ttlUpdated ← redis.expire(sessionKey, sessionTimeout)
      value ← redis.get[String](sessionKey)
    } yield value.map(UserID(_))
  }

  private def redisKey(sessionID:String) = s"$prefix:$sessionID"
  private def generateSessionID(userID: UserID):String = UUID.randomUUID().toString
}
