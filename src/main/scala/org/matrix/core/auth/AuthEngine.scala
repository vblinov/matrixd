package org.matrix.core.auth

import org.matrix.protocol.base.UserID
import org.matrix.protocol.login.LoginFlows
import org.matrix.protocol.login.LoginRequest.AuthData

import scala.concurrent.Future

class AuthEngine(authenticators:Seq[Authenticator] = Seq.empty) {
  lazy val description = LoginFlows(authenticators.map(_.describe))

  def register(authData: AuthData):Future[Option[UserID]] = {
    authenticators.find(_.`type` == authData.`type`) match {
      case Some(authenticator) ⇒ authenticator.register(authData.credentials)
      case None ⇒ Future.failed(new IllegalArgumentException("Registration type not found"))
    }
  }
  def challenge(authData: AuthData):Future[Option[UserID]] = {
    authenticators.find(_.`type` == authData.`type`) match {
      case Some(authenticator) ⇒ authenticator.challenge(authData.credentials)
      case None ⇒ Future.failed(new IllegalArgumentException("Login type not found"))
    }
  }
}