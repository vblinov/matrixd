package org.matrix.core.auth

import org.matrix.protocol.base.UserID
import org.matrix.protocol.login.LoginInfo

import scala.concurrent.Future

trait Authenticator {
  def `type`:String
  def stages:List[String] = "identify" :: Nil

  def register(credentials:Map[String, String]):Future[Option[UserID]]
  def challenge(credentials:Map[String, String]):Future[Option[UserID]]

  def describe = LoginInfo(stages, `type`)
}
