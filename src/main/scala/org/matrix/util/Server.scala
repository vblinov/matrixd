package org.matrix.util

trait Server {
  def start():Unit
  def shutdown():Unit
}
