package org.matrix.util

import com.typesafe.scalalogging.slf4j.Logger
import org.slf4j.LoggerFactory

trait Logging {
  protected final lazy val log = Logger(LoggerFactory.getLogger(loggerName))
  protected def loggerName = getClass.getName
}
