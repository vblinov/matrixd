package org.matrix.util

import scala.util.matching.Regex

trait RegexMatching {
  implicit class MatrixStringContext(val sc: StringContext) {
    object r {
      def apply(args : Any*): Regex = sc.s(args : _*).r

      def unapplySeq(s: String): Option[Seq[String]] = {
        val regexp = sc.parts.mkString ("(.+)").r
        regexp.unapplySeq(s)
      }
    }
  }
}