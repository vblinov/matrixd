package org.matrix.util

import akka.actor.{Status, Actor}

trait ReplyingActor extends Actor {
  def receive = respondWith(respond)

  def respond:ReplyingActor.Respond
  final def respondWith(responder: ReplyingActor.Respond) = new PartialFunction[Any,Unit] {
    override def isDefinedAt(x: Any): Boolean = responder.isDefinedAt(x)
    override def apply(v1: Any): Unit = try {
      sender() ! responder.apply(v1)
    } catch {
      case ex: Throwable ⇒
        sender ! Status.Failure(ex)
        throw ex
    }
  }
}

object ReplyingActor {
  type Respond = PartialFunction[Any, Any]
}