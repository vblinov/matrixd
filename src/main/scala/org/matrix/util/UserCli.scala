package org.matrix.util

import scala.annotation.tailrec
import scala.io.StdIn

trait UserCli {
  def awaitForShutdownCommand(app: Server) = {
    println("Type 'q' to quit.")
    commandLoop(app)
  }

  @tailrec private def commandLoop(app: Server): Unit = StdIn.readLine() match {
    case "q" =>
      println("exiting...")
      app.shutdown()
    case _ =>
      println("Illegal Command!")
      commandLoop(app)
  }
}
