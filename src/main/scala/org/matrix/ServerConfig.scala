package org.matrix

import ammonite.sshd.Creds

import scala.util.Random

object ServerConfig {
  val debug = true
  val domain = "localhost"

  object Http {
    val port = 8080
  }

  object Repl {
    def port = 2222
    def credentials = Creds(username, password)
    def username = System.getProperty("user.name")
    def password = Random.alphanumeric.take(8).mkString
  }
}
