import akka.actor.ActorSystem
import org.matrix.core._
import org.matrix.core.db.{Database, TempH2Connection}

import scala.language.postfixOps

object matrixd {
  lazy val app = new Core with WebServer with RedisConnection with DatabaseAccess with Repl {
    override lazy val db = new Database with TempH2Connection
    override implicit lazy val actorSystem: ActorSystem = ActorSystem("matrixd")

    override def start() = {
      db.runBlocking(db.createSchema)
      super.start()
    }
  }

  def main(args:Array[String]) = {
    app.start()
  }
}
